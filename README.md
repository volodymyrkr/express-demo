# express-demo v.0.0.0

1. Configure your filletree 

     - create gitlab folder
    ```
    mkdir gitlab
    ```
     
     - navigate to gitlab folder
    ``` 
    cd gitlab
    ``` 

2. Configure your SSH key 

     - create your SSH key [more details...](https://superuser.com/questions/232373/how-to-tell-git-which-private-key-to-use)
    ``` 
    ssh-keygen -t rsa
    ``` 
     - copy your SSH key to system folder ```~/.ssh```
     
     - add SSH using config into file ```~/.ssh/config```
     
    ``` 
    host gitlab.com
     HostName gitlab.com
     IdentityFile ~/.ssh/volodymyrkr_rsa
     User git
    ```
    
    - add your SSH key to ssh-agent [more details...](https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#adding-your-ssh-key-to-the-ssh-agent)
    ```
    ssh-add -K ~/.ssh/volodymyrkr_rsa
    ```

3. Create your repository on remote side [Gitlab](https://gitlab.com/projects/new)

4. Clone your repositry into local folder via ssh

    ```
    git clone git@gitlab.com:volodymyrkr/express-demo.git
    git fetch && git pull
    ```
5. Create file ```.gitignore``` and add all folder you wanna ignore

6. Make your changes add commit pull and enjoy :) 

P.S. READDME.md commands description can be found at [Github Help](https://help.github.com/en/articles/basic-writing-and-formatting-syntax) 
 
# Create boilerplate express + node + ts 

1. Webpack solution 
    - [Webpack more details](https://medium.com/the-andela-way/how-to-set-up-an-express-api-using-webpack-and-typescript-69d18c8c4f52)
    - [Warning solution more details](https://www.npmjs.com/package/webpack-node-externals)

2. Grun solution
    - [Grunt More details](https://brianflove.com/2016/11/08/typescript-2-express-node/)


# Run application


```
yarn start:dev
``` 

or 

```
npm run start:dev
```